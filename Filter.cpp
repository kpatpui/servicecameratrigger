// Filter.cpp: implementation of the Filter class.
//
//////////////////////////////////////////////////////////////////////

#include "Filter.h"
#include "TextOperation.hpp"


#include "SttTmpl.hpp"
#include "trcerr_e.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

	Filter::Filter()
	{
		msgID = 123;
	}

	Filter::~Filter()
	{

	}
	void Filter::SetId(int msgID){
		this->msgID = msgID;
	}
	void Filter::SetDesc(string msgDesc){
		this->msgDesc = msgDesc;
	}
	void Filter::SetisCapDev1(bool isCapDev1){
		this->isCapDev1 = isCapDev1;
	}
	void Filter::SetisCapDev2(bool isCapDev2){
		this->isCapDev2 = isCapDev2;
	}
	void Filter::SetisCapDev3(bool isCapDev3){
		this->isCapDev3 = isCapDev3;
	}
	void Filter::SetisCapDev4(bool isCapDev4){
		this->isCapDev4 = isCapDev4;
	}
	void Filter::SetTranType(string TranType){
				this->TranType = TranType;
	}
	int Filter::GetID(){
		return this->msgID;
	}
	string Filter::GetDesc(){
		return this->msgDesc;
	}
	bool Filter::GetisCapDev1(){
		return this->isCapDev1;
	}
	bool Filter::GetisCapDev2(){
		return this->isCapDev2;
	}
	bool Filter::GetisCapDev3(){
		return this->isCapDev3;
	}
	bool Filter::GetisCapDev4(){
		return this->isCapDev4;
	}
	string Filter::GetTranType(){
		return this->TranType;
	}
	// region Filter.ini
	//vector<Filter> Filter::ImplementFilter(char filterPath[])
	//{
	//	vector<Filter> vFilter;
	//	ifstream inFile (filterPath);
	//	string line;
	//	int linenum = 0;
	//	while (getline (inFile, line)){
	//		Filter tmpFilter;
	//		linenum++;
	//		istringstream linestream(line);
	//		//Skip ; comment
	//		if(line[0]==';' || line[0] == '#' || line[0]==NULL || line[0]==' ')
	//			continue;
	//		string item;
	//		int itemnum = 0;
	//		while (getline (linestream, item, ',')) {
	//			itemnum++;
	//			switch(itemnum)
	//			{
	//				case 1:
	//					//cout << "Msg ID : " << item << endl;
	//					tmpFilter.SetId(atoi(item.c_str()));
	//				break;
	//				case 2:
	//					//cout << "Msg Desc : " << item << endl;
	//					tmpFilter.SetDesc(item.c_str());
	//				break;
	//				case 3:
	//					//cout << "Capture Device1 : " << item << endl;
	//					tmpFilter.SetisCapDev1(atoi(item.c_str()));
	//				break;
	//				case 4:
	//					//cout << "Capture Device2 : " << item << endl;
	//					tmpFilter.SetisCapDev2(atoi(item.c_str()));
	//				break;
	//				case 5:
	//					//cout << "Capture Device3 : " << item << endl;
	//					tmpFilter.SetisCapDev3(atoi(item.c_str()));
	//				break;
	//				case 6:
	//					//cout << "Capture Device4 : " << item << endl;
	//					tmpFilter.SetisCapDev4(atoi(item.c_str()));
	//				break;
	//				case 7:
	//					//cout << "Tran Type : " << item << endl;
	//					tmpFilter.SetTranType(item.c_str());
	//				break;
	//				default:
	//					// do nothing
	//				break;
	//			}
	//		}	
	//		vFilter.push_back(tmpFilter);		
	//	}	
	//	return vFilter;
	//}

	// end region Filter.ini

	vector<Filter> Filter::ImplementFilter()
	{
		TrcWritef(TRC_FUNC, "> Filter::ImplementFilter()");
		vector<Filter> vFilter;
		string line;
		int linenum = 0;

		HKEY hKey;
		SHORT lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		   SOFTWARE_CVService_Filter,
		   0, KEY_QUERY_VALUE, &hKey );

		if( lRet != ERROR_SUCCESS )
		{
			string strMsg = "Error cannot read reg: ";
			char buffMsg[1024];
			sprintf(buffMsg, "%s",strMsg.c_str());
			//WriteToLog ('e',buffMsg);
		}
		else
		{
			 return Filter::QueryFilterKey(hKey);

		}	
		TrcWritef(TRC_FUNC, "< Filter::ImplementFilter()");
		return vFilter;
	}
	vector<Filter>  Filter::QueryFilterKey(HKEY hKey) 
	{ 
		TrcWritef(TRC_FUNC, ">> Filter::QueryFilterKey");
		TCHAR    achKey[MAX_KEY_LENGTH];   // buffer for subkey name
		DWORD    cbName;                   // size of name string 
		TCHAR    achClass[MAX_PATH] = TEXT("");  // buffer for class name 
		DWORD    cchClassName = MAX_PATH;  // size of class string 
		DWORD    cSubKeys=0;               // number of subkeys 
		DWORD    cbMaxSubKey;              // longest subkey size 
		DWORD    cchMaxClass;              // longest class string 
		DWORD    cValues;              // number of values for key 
		DWORD    cchMaxValue;          // longest value name 
		DWORD    cbMaxValueData;       // longest value data 
		DWORD    cbSecurityDescriptor; // size of security descriptor 
		FILETIME ftLastWriteTime;      // last write time 
		
		vector<Filter> vFilter;

		DWORD i, retCode; 
	 
		TCHAR  achValue[MAX_VALUE_NAME]; 
		DWORD cchValue = MAX_VALUE_NAME; 
	 
		// Get the class name and the value count. 
		retCode = RegQueryInfoKey(
			hKey,                    // key handle 
			achClass,                // buffer for class name 
			&cchClassName,           // size of class string 
			NULL,                    // reserved 
			&cSubKeys,               // number of subkeys 
			&cbMaxSubKey,            // longest subkey size 
			&cchMaxClass,            // longest class string 
			&cValues,                // number of values for this key 
			&cchMaxValue,            // longest value name 
			&cbMaxValueData,         // longest value data 
			&cbSecurityDescriptor,   // security descriptor 
			&ftLastWriteTime);       // last write time 
	    
		// Enumerate the key values. 
		if (cValues) 
		{
			for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++) 
			{ 
				cchValue = MAX_VALUE_NAME; 
				achValue[0] = '\0'; 
				retCode = RegEnumValue(hKey, i, 
					achValue, 
					&cchValue, 
					NULL, 
					NULL,
					NULL,
					NULL);
	 
				if (retCode == ERROR_SUCCESS ) 
				{ 
					// get key value
					char tmpFilterText[1024];
					TextOperation::GetFilterRegValue(tmpFilterText,achValue);

					Filter *tmpFilter = new Filter;
					//linenum++;
					istringstream linestream(tmpFilterText);
					//Skip ; comment
					if(tmpFilterText[0]==';' || tmpFilterText[0] == '#' || tmpFilterText[0]==NULL || tmpFilterText[0]==' ')
					{
						continue;
					}
					else
					{
						string item;
						int itemnum = 0;
						tmpFilter->SetId(atoi(achValue));

						while (getline (linestream, item, ',')) {
							itemnum++;
							switch(itemnum)
							{
								case 1:
									//cout << "Msg Desc : " << item << endl;
									tmpFilter->SetDesc(TextOperation::StringToUpper(item.c_str()));
								break;
								case 2:
									//cout << "Capture Device1 : " << item << endl;
									tmpFilter->SetisCapDev1(atoi(item.c_str()));
								break;
								case 3:
									//cout << "Capture Device2 : " << item << endl;
									tmpFilter->SetisCapDev2(atoi(item.c_str()));
								break;
								case 4:
									//cout << "Capture Device3 : " << item << endl;
									tmpFilter->SetisCapDev3(atoi(item.c_str()));
								break;
								case 5:
									//cout << "Capture Device4 : " << item << endl;
									tmpFilter->SetisCapDev4(atoi(item.c_str()));
								break;
								case 6:
									//cout << "Tran Type : " << item << endl;
									tmpFilter->SetTranType(item.c_str());
								break;
								default:
									// do nothing
								break;
							}
						}	
						vFilter.push_back(*tmpFilter);		
					} 
					delete(tmpFilter);
				}
			}
		}
		TrcWritef(TRC_FUNC, "<< Filter::QueryFilterKey");
		return vFilter;
	}