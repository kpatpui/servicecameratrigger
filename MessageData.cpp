
#include "MessageData.hpp"
#include "SttTmpl.hpp"
#include "trcerr_e.h"

// TODO: Msg step flow
// 1. at every app start sent camera status to host
// 2. at every tran end, check camera status at registry
//	2.1 status not change -> do nothing
//	2.2 status change -> send msg

	int CAMERA_LOG_MSG_NUMBER = 9050;

MessageData::MessageData()
{
	TrcWritef(TRC_INFO, "> MessageData::Constructor");
}

MessageData::~MessageData(void)
{
	TrcWritef(TRC_INFO, "> MessageData::Destructor");
}

SHORT SendMSGToHost()
{
	SHORT sRc=0;
	NdcDdcApplicationFW APPL_FW;
	
	sRc = APPL_FW.TransactionHandlerRequest("T2HUnsolicitedCamError");
	
	return sRc;
}
SHORT MessageData::SendMsgToHost(CHAR *msg)
{
	SHORT sRc=0;
	NdcDdcApplicationFW APPL_FW;
		
	sRc = APPL_FW.TransactionHandlerRequest(msg);
	//APPL_FW.send
	return sRc;
}

SHORT MessageData::WriteJournal(int msg,CCJournalFW *jourFw)
{
	TrcWritef(TRC_FUNC, "> MessageData::WriteJournal(%d)",msg);
	SHORT sRc=0;
	sRc = jourFw->Write(msg);
	TrcWritef(TRC_FUNC, "< MessageData::WriteJournal(%d)",msg);
	return sRc;
}

BOOL MessageData::CheckStatusChange(TextOperation *txtObj, CCJournalFW *jourFw, CCVarFW *varFw)
{
	TrcWritef(TRC_FUNC, "> MessageData::CheckStatusChange(TextOperation *txtObj,CCJournalFW jourFw, CCVarFW varFw)");
	BOOL isFoundCameraToSend = false;
	const string  T2HUnsolicitedCamError = "T2HUnsolicitedCamError";
	char buffMsgC1[1024];
	
	sprintf(buffMsgC1, "<<-------------StartPvEvent------------->>");
	txtObj->WriteToLog (buffMsgC1);
	// camera 1 check
	if(strcmp(txtObj->ENABLE_CAMERA_1,"TRUE") == 0)
	{
		string sCamera1Status = txtObj->Camera1Status;
		sCamera1Status = sCamera1Status.substr(1,1);
		sprintf(buffMsgC1, "sCamera1Status: %s, %s",txtObj->Camera1Status, sCamera1Status.c_str());
		txtObj->WriteToLog (buffMsgC1);

		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC1);
		varFw->SetVar("CAM1_STATUS",(PCHAR)sCamera1Status.c_str());
		isFoundCameraToSend = true;
	}

	char buffMsgC2[1024];
	if(strcmp(txtObj->ENABLE_CAMERA_2,"TRUE") == 0)
	{
		string sCamera2Status = txtObj->Camera2Status;
		sCamera2Status = sCamera2Status.substr(1,1);
		sprintf(buffMsgC2, "sCamera2Status: %s, %s",txtObj->Camera2Status, sCamera2Status.c_str());
		txtObj->WriteToLog (buffMsgC2);
		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC2);
		varFw->SetVar("CAM2_STATUS",(PCHAR)sCamera2Status.c_str());
		isFoundCameraToSend = true;
	}
	if(strcmp(txtObj->ENABLE_CAMERA_3,"TRUE") == 0)
	{
		char buffMsgC3[1024];
		string sCamera3Status = txtObj->Camera3Status;
		sCamera3Status = sCamera3Status.substr(1,1);
		sprintf(buffMsgC3, "sCamera3Status: %s, %s",txtObj->Camera3Status, sCamera3Status.c_str());
		txtObj->WriteToLog (buffMsgC3);
		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC3);
		varFw->SetVar("CAM3_STATUS",(PCHAR)sCamera3Status.c_str());
		isFoundCameraToSend = true;
	}

	if(strcmp(txtObj->ENABLE_CAMERA_4,"TRUE") == 0)
	{
		char buffMsgC4[1024];
		string sCamera4Status = txtObj->Camera4Status;
		sCamera4Status = sCamera4Status.substr(1,1);
		sprintf(buffMsgC4, "sCamera4Status: %s, %s",txtObj->Camera4Status, sCamera4Status.c_str());
		txtObj->WriteToLog (buffMsgC4);
		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC4);
		varFw->SetVar("CAM4_STATUS",(PCHAR)sCamera4Status.c_str());
		isFoundCameraToSend = true;
	}

	sprintf(buffMsgC1, "<<-------------EndPvEvent------------->>");
	txtObj->WriteToLog (buffMsgC1);
	TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange(TextOperation *txtObj,CCJournalFW jourFw, CCVarFW varFw)");

	return isFoundCameraToSend;


	//
	//if((atoi(txtObj->DEVCON_COUNT) == 0) && (strcmp(txtObj->DEVCON_FLAG,"FALSE") == 0))
	//{
	//	txtObj->SetRegValue("TRUE","DEVCON_FLAG");
	//	TrcWritef(TRC_FUNC, ">> MessageData::SetRegValue->DEVCON_FLAG = TRUE");
	//}

	//if(isFoundCameraError)
	//{
	//	TrcWritef(TRC_FUNC, ">> MessageData::FoundCameraError");
	//	if((strcmp(txtObj->ENABLE_SEND_TO_HOST,"TRUE") == 0) && (strcmp(txtObj->DEVCON_FLAG,"TRUE") == 0))
	//	{
	//		TrcWritef(TRC_FUNC, ">> ENABLE_SEND_TO_HOST = True, DEVCON_FLAG = True");
	//		if(atoi(txtObj->DEVCON_COUNT) == atoi(txtObj->MAX_DEVCON))
	//		{
	//			TrcWritef(TRC_FUNC, ">>MessageData::WriteJournal->%d",CAMERA_LOG_MSG_NUMBER);
	//			WriteJournal(CAMERA_LOG_MSG_NUMBER,jourFw);
	//			TrcWritef(TRC_FUNC, ">>MessageData::SendMSGToHost->%d",CAMERA_LOG_MSG_NUMBER);
	//			SendMSGToHost();
	//			
	//			// ENABLE_SEND_TO_HOST
	//			txtObj->SetRegValue("FALSE","DEVCON_FLAG");
	//		}
	//	}
	//}
	//sprintf(buffMsgC1, "<<-------------EndPvEvent------------->>");
	//txtObj->WriteToLog (buffMsgC1);
	//TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange(TextOperation *txtObj,CCJournalFW jourFw, CCVarFW varFw)");
	//return FALSE;
}

//BOOL MessageData::CheckStatusChange(TextOperation *txtObj,CCJournalFW *jourFw, CCVarFW *varFw)
//{
//	TrcWritef(TRC_FUNC, "> MessageData::CheckStatusChange(TextOperation *txtObj,CCJournalFW jourFw, CCVarFW varFw)");
//	BOOL isFoundCameraToSend = false;
//	BOOL isFoundCameraError = false;
//	int IDLE_MSG_NUMBER = 9000;
//	int CAMERA_LOG_MSG_NUMBER = 9050;
//	const string  T2HUnsolicitedCamError = "T2HUnsolicitedCamError";
//
//	char buffMsgC1[1024];
//
//	sprintf(buffMsgC1, "<<-------------StartPvEvent------------->>");
//	txtObj->WriteToLog (buffMsgC1);
//
//	if(strcmp(txtObj->ENABLE_CAMERA_1,"TRUE") == 0)
//	{
//		string sCamera1Status = txtObj->Camera1Status;
//		if(sCamera1Status.compare("00"))
//		{
//			isFoundCameraError = true;
//		}
//		sCamera1Status = sCamera1Status.substr(1,1);
//		sprintf(buffMsgC1, "sCamera1Status: %s, %s",txtObj->Camera1Status, sCamera1Status.c_str());
//		txtObj->WriteToLog (buffMsgC1);
//
//		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC1);
//		varFw->SetVar("CAM1_STATUS",(PCHAR)sCamera1Status.c_str());
//		isFoundCameraToSend = true;
//	}
//
//	char buffMsgC2[1024];
//	if(strcmp(txtObj->ENABLE_CAMERA_2,"TRUE") == 0)
//	{
//		string sCamera2Status = txtObj->Camera2Status;
//		if(sCamera2Status.compare("00"))
//		{
//			isFoundCameraError = true;
//		}
//		sCamera2Status = sCamera2Status.substr(1,1);
//		sprintf(buffMsgC2, "sCamera2Status: %s, %s",txtObj->Camera2Status, sCamera2Status.c_str());
//		txtObj->WriteToLog (buffMsgC2);
//		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC2);
//		varFw->SetVar("CAM2_STATUS",(PCHAR)sCamera2Status.c_str());
//		isFoundCameraToSend = true;
//	}
//	if(strcmp(txtObj->ENABLE_CAMERA_3,"TRUE") == 0)
//	{
//		char buffMsgC3[1024];
//		string sCamera3Status = txtObj->Camera3Status;
//		if(sCamera3Status.compare("00"))
//		{
//			isFoundCameraError = true;
//		}
//		sCamera3Status = sCamera3Status.substr(1,1);
//		sprintf(buffMsgC3, "sCamera3Status: %s, %s",txtObj->Camera3Status, sCamera3Status.c_str());
//		txtObj->WriteToLog (buffMsgC3);
//		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC3);
//		varFw->SetVar("CAM3_STATUS",(PCHAR)sCamera3Status.c_str());
//		isFoundCameraToSend = true;
//	}
//
//	if(strcmp(txtObj->ENABLE_CAMERA_4,"TRUE") == 0)
//	{
//		char buffMsgC4[1024];
//		string sCamera4Status = txtObj->Camera4Status;
//		if(sCamera4Status.compare("00"))
//		{
//			isFoundCameraError = true;
//		}
//		sCamera4Status = sCamera4Status.substr(1,1);
//		sprintf(buffMsgC4, "sCamera4Status: %s, %s",txtObj->Camera4Status, sCamera4Status.c_str());
//		txtObj->WriteToLog (buffMsgC4);
//		TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange->%s",buffMsgC4);
//		varFw->SetVar("CAM4_STATUS",(PCHAR)sCamera4Status.c_str());
//		isFoundCameraToSend = true;
//	}
//
//	if(isFoundCameraToSend)
//	{
//		WriteJournal(IDLE_MSG_NUMBER,jourFw);
//	}
//	
//	if((atoi(txtObj->DEVCON_COUNT) == 0) && (strcmp(txtObj->DEVCON_FLAG,"FALSE") == 0))
//	{
//		txtObj->SetRegValue("TRUE","DEVCON_FLAG");
//		TrcWritef(TRC_FUNC, ">> MessageData::SetRegValue->DEVCON_FLAG = TRUE");
//	}
//
//	if(isFoundCameraError)
//	{
//		TrcWritef(TRC_FUNC, ">> MessageData::FoundCameraError");
//		if((strcmp(txtObj->ENABLE_SEND_TO_HOST,"TRUE") == 0) && (strcmp(txtObj->DEVCON_FLAG,"TRUE") == 0))
//		{
//			TrcWritef(TRC_FUNC, ">> ENABLE_SEND_TO_HOST = True, DEVCON_FLAG = True");
//			if(atoi(txtObj->DEVCON_COUNT) == atoi(txtObj->MAX_DEVCON))
//			{
//				TrcWritef(TRC_FUNC, ">>MessageData::WriteJournal->%d",CAMERA_LOG_MSG_NUMBER);
//				WriteJournal(CAMERA_LOG_MSG_NUMBER,jourFw);
//				TrcWritef(TRC_FUNC, ">>MessageData::SendMSGToHost->%d",CAMERA_LOG_MSG_NUMBER);
//				SendMSGToHost();
//				
//				// ENABLE_SEND_TO_HOST
//				txtObj->SetRegValue("FALSE","DEVCON_FLAG");
//			}
//		}
//	}
//	sprintf(buffMsgC1, "<<-------------EndPvEvent------------->>");
//	txtObj->WriteToLog (buffMsgC1);
//	TrcWritef(TRC_FUNC, "< MessageData::CheckStatusChange(TextOperation *txtObj,CCJournalFW jourFw, CCVarFW varFw)");
//	return FALSE;
//}

// wtite ej

/*
	idle ej, every idle
	error status
	- in app start
	- after exit sop
	- first time when found that camera error

*/
// send msg login

/*
	bool ENABLE_SEND_TO_HOST = false;
	bool DEVCON_FLAG = false;
*/

BOOL MessageData::CheckConditionForSendMsgToHost(TextOperation *txtObj, CCJournalFW *jourFw)
{
	BOOL bIsSendMsg = FALSE;

	TrcWritef(TRC_FUNC, ">> MessageData::FoundCameraError");
	if((strcmp(txtObj->DEVCON_FLAG,"TRUE") == 0))
	{
		TrcWritef(TRC_FUNC, ">>MessageData::CheckConditionForSendMsgToHost DEVCON_FLAG = True");
		if(atoi(txtObj->DEVCON_COUNT) == atoi(txtObj->MAX_DEVCON))
		{
			TrcWritef(TRC_FUNC, ">>MessageData::WriteJournal->%d",CAMERA_LOG_MSG_NUMBER);
			TrcWritef(TRC_FUNC, ">>MessageData::SendMSGToHost->%d",CAMERA_LOG_MSG_NUMBER);
			
			if(strcmp(txtObj->ENABLE_SEND_TO_HOST,"TRUE") == 0)
			{
				bIsSendMsg = TRUE;
			}

			// set status to false after send.
			txtObj->SetRegValue("FALSE","DEVCON_FLAG");
			TrcWritef(TRC_FUNC, ">>MessageData::CheckConditionForSendMsgToHost Set DEVCON_FLAG = FALSE");
		}

		TrcWritef(TRC_FUNC, ">> ENABLE_SEND_TO_HOST = True, DEVCON_FLAG = True");
		TrcWritef(TRC_FUNC, "<<MessageData::CheckConditionForSendMsgToHost");
	}
	
	return bIsSendMsg;
}
BOOL MessageData::CheckErrorStatusChange(TextOperation *txtObj)
{
	BOOL bIsCameraError = FALSE;
	TrcWritef(TRC_FUNC, "> MessageData::CheckErrorStatusChange");
		
	if((atoi(txtObj->DEVCON_COUNT) == 0) && (strcmp(txtObj->DEVCON_FLAG,"FALSE") == 0))
	{
		txtObj->SetRegValue("TRUE","DEVCON_FLAG");
		TrcWritef(TRC_FUNC, ">> MessageData::SetRegValue->DEVCON_FLAG = TRUE");
	}

	if(strcmp(txtObj->ENABLE_CAMERA_1,"TRUE") == 0)
	{
		string sCamera1Status = txtObj->Camera1Status;
		if(sCamera1Status.compare("00") != 0)
		{
			bIsCameraError = TRUE;
		}
	}
	if(strcmp(txtObj->ENABLE_CAMERA_2,"TRUE") == 0)
	{
		string sCamera2Status = txtObj->Camera2Status;
		if(sCamera2Status.compare("00") != 0)
		{
			bIsCameraError = TRUE;
		}
	}
	if(strcmp(txtObj->ENABLE_CAMERA_3,"TRUE") == 0)
	{
		string sCamera3Status = txtObj->Camera3Status;
		if(sCamera3Status.compare("00") != 0)
		{
			bIsCameraError = TRUE;
		}
	}

	if(strcmp(txtObj->ENABLE_CAMERA_4,"TRUE") == 0)
	{
		string sCamera4Status = txtObj->Camera4Status;
		if(sCamera4Status.compare("00") != 0)
		{
			bIsCameraError = TRUE;
		}
	}
	TrcWritef(TRC_FUNC, "> MessageData::CheckErrorStatusChange return %d",bIsCameraError);

	return bIsCameraError;
}