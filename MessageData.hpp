#include "SttTmpl.hpp"
#include "TextOperation.hpp"
#include <stdio.h>

#define CAMERA_STATUS_OK 0
#define CAMERA_STATUS_ERROR 1
class MessageData
{
	public:
		
		// Con & De struc
		MessageData(void);
		~MessageData(void);
		
		// Public Method
		/*SHORT SendMSGToHost();
		SHORT SendMSGToHost(CCString msg);*/
		SHORT MessageData::SendMsgToHost(CHAR *msgId);
		SHORT MessageData::WriteJournal(int msg,CCJournalFW *jourFw);
		BOOL MessageData::CheckStatusChange(TextOperation *txtObj, CCJournalFW *jourFw, CCVarFW *varFw);
		BOOL MessageData::CheckErrorStatusChange(TextOperation *txtObj);
		std::string MessageData::GetCameraLogStatus(string status);
		BOOL MessageData::CheckConditionForSendMsgToHost(TextOperation *txtObj, CCJournalFW *jourFw);
		
};
