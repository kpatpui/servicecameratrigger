#include "TextOperation.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <sstream>
#include <vector>

#include "SttTmpl.hpp"
#include "trcerr_e.h"
#include "Filter.h"
#include <exception>
#include "time.h"

// local prototype
std::string IntToString ( int number );
const std::string currentDateTime();
const std::string currentDate();
string GenerateLogMsg(char strMsg[]);
string GenerateErrorLogMsg(exception& e);
char *Trimwhitespace(char *str);

bool TextOperation::WriteXml(Filter filterObj, std::string date, std::string time, std::string desc, std::string terminalID)
{
	int iC1 = filterObj.GetisCapDev1();
	int iC2 = filterObj.GetisCapDev2();
	int iC3 = filterObj.GetisCapDev3();
	int iC4 = filterObj.GetisCapDev4();
	int iCodeId = filterObj.GetID();

	std::string code = IntToString(iCodeId);
	std::string tranType = filterObj.GetTranType();
	std::string sCamera1 = IntToString(iC1);
	std::string sCamera2 = IntToString(iC2);
	std::string sCamera3 = IntToString(iC3);
	std::string sCamera4 = IntToString(iC4);
	
	std::string sUpperDesp = TextOperation::StringToUpper(desc.erase(desc.find_last_not_of(" \n\r\t")+1));

	std::string START_TRANSACTION = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
			"<Transactions>\n"
			"<Transaction MachineNo=\""+terminalID.erase(terminalID.find_last_not_of(" \n\r\t")+1)+"\">\n"
			"<Code>"+code.c_str()+"</Code>\n"
			"<Desc>"+sUpperDesp+"</Desc>\n"
			"<Camera1>"+sCamera1.c_str()+"</Camera1>\n"
			"<Camera2>"+sCamera2.c_str()+"</Camera2>\n"
			"<Camera3>"+sCamera3.c_str()+"</Camera3>\n"
			"<Camera4>"+sCamera4.c_str()+"</Camera4>\n"
			"<Type>"+tranType.c_str()+"</Type>\n"
			"</Transaction>\n"
			"</Transactions>\n";
	//TrcWritef(TRC_FUNC, "> TextOperation::WriteXml After code: %s",code.c_str());
	
	std::ofstream file(XML_PATH); 
    file<<START_TRANSACTION;       //write to it
    file.close();
	
    return 0;
}


long TextOperation::GetRegValue(char *pValue,char *keys)
{
	HKEY hKey;
    char szProductType[80];
    DWORD dwBufLen=80;
	LONG lRet;

    lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
       SOFTWARE_CVService,
       0, KEY_QUERY_VALUE, &hKey );

    if( lRet != ERROR_SUCCESS )
    {
		string strMsg = "Error cannot read reg: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s",strMsg.c_str());
		WriteToLog (buffMsg);
	}
	else
	{

		lRet = RegQueryValueEx( hKey, keys, NULL, NULL,
			   (LPBYTE)szProductType, &dwBufLen);
	}
	
    if( (lRet != ERROR_SUCCESS) || (dwBufLen > 80) )
	{
		string strMsg = "Error cannot read reg value: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s",strMsg.c_str());
		WriteToLog (buffMsg);
	}
	else
	{
		strcpy(pValue,szProductType);
	}

    lRet = RegCloseKey( hKey );
	return(lRet);

}
long TextOperation::SetRegValue(char *pValue,char *keys)
{
	HKEY hKey;
    char szProductType[80];
    DWORD dwBufLen=80;
	LONG lRet;

    lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
       SOFTWARE_CVService,
       0, KEY_SET_VALUE  , &hKey );

    if( lRet != ERROR_SUCCESS )
    {
		string strMsg = "Error cannot read reg: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s %s %d",strMsg.c_str(),GetLastError(), sizeof(pValue));
		WriteToLog (buffMsg);
	}
	else
	{
		lRet = RegSetValueEx(hKey, keys, 0, REG_SZ, (LPBYTE)pValue, strlen(pValue));
	}
	
    if( (lRet != ERROR_SUCCESS) || (dwBufLen > 80) )
	{
		string strMsg = "Error cannot write reg value: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s %s %d",strMsg.c_str(),GetLastError(), strlen(pValue));
		WriteToLog (buffMsg);
	}

    lRet = RegCloseKey( hKey );
	return(lRet);

}
long TextOperation::GetFilterRegValue(char *pValue,char *keys)
{
	HKEY hKey;
    char szProductType[80];
    DWORD dwBufLen=80;
	LONG lRet;
	TextOperation *txtObj = new TextOperation;

    lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
       SOFTWARE_CVService_Filter,
       0, KEY_QUERY_VALUE, &hKey );

    if( lRet != ERROR_SUCCESS )
    {
		string strMsg = "Error cannot read reg: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s",strMsg.c_str());
		txtObj->WriteToLog (buffMsg);
	}
	else
	{

		lRet = RegQueryValueEx( hKey, keys, NULL, NULL,
			   (LPBYTE)szProductType, &dwBufLen);
	}
	
    if( (lRet != ERROR_SUCCESS) || (dwBufLen > 80) )
	{
		string strMsg = "Error cannot read reg value: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s",strMsg.c_str());
		txtObj->WriteToLog (buffMsg);
	}
	else
	{
		strcpy(pValue,szProductType);
	}

    lRet = RegCloseKey( hKey );
	if(txtObj != NULL)
	{
		delete(txtObj);
	}
	return(lRet);

}
long TextOperation::GetTerminalIdRegValue(char *pValue,char *keys)
{
	HKEY hKey;
    char szProductType[80];
    DWORD dwBufLen=80;
	LONG lRet;

    lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
       SOFTWARE_Terminal_Addr,
       0, KEY_QUERY_VALUE, &hKey );

    if( lRet != ERROR_SUCCESS )
    {
		string strMsg = "Error cannot open reg: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s (%d)",strMsg.c_str(),GetLastError());
		WriteToLog (buffMsg);
	}
	else
	{

		lRet = RegQueryValueEx( hKey, keys, NULL, NULL,
			   (LPBYTE)szProductType, &dwBufLen);
	}
	
    if( (lRet != ERROR_SUCCESS) || (dwBufLen > 80) )
	{
		string strMsg = "Error cannot read reg value: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s (%d)",strMsg.c_str(),GetLastError());
		WriteToLog (buffMsg);
	}
	else
	{
		strcpy(pValue,szProductType);
	}

    lRet = RegCloseKey( hKey );
	return(lRet);

}
long TextOperation::GetCameraStatusRegValue(char *pValue,char *keys)
{
	HKEY hKey;
    char szProductType[80];
    DWORD dwBufLen=80;
	LONG lRet;

    lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
       SOFTWARE_CVService_Status,
       0, KEY_QUERY_VALUE, &hKey );

    if( lRet != ERROR_SUCCESS )
    {
		string strMsg = "Error cannot open reg: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s (%d)",strMsg.c_str(),GetLastError());
		WriteToLog (buffMsg);
	}
	else
	{

		lRet = RegQueryValueEx( hKey, keys, NULL, NULL,
			   (LPBYTE)szProductType, &dwBufLen);
	}
	
    if( (lRet != ERROR_SUCCESS) || (dwBufLen > 80) )
	{
		string strMsg = "Error cannot read reg value: ";
		strMsg.append(keys);
		char buffMsg[1024];
		sprintf(buffMsg, "%s (%d)",strMsg.c_str(),GetLastError());
		WriteToLog (buffMsg);
	}
	else
	{
		strcpy(pValue,szProductType);
	}

    lRet = RegCloseKey( hKey );
	return(lRet);

}
int TextOperation::WriteToLog(char stringMsg[])
{
	FILE* log;
	try
    {
		
		string tmpLogFilePath = TextOperation::LOG_FILE_PATH+currentDate()+TextOperation::LOG_FILE_NAME;
		const char * logFilePath = tmpLogFilePath.c_str();
		log = fopen(logFilePath, "a+");
		if (log == NULL)
		{
			return -1;
		}
		fprintf(log, "%s\n", GenerateLogMsg(stringMsg).c_str());
	}
	catch (exception& e)
	{
		// TODO: enable code on dll
		TrcWritef(TRC_FUNC, "> %s\n",GenerateErrorLogMsg(e).c_str());
	}
	
	fclose(log);
	return 0;

}

string GenerateLogMsg(char strMsg[])
{
	char buffMsg[1024];
	string returnMsg;
	// 2014/09/30 11:11:11 --> T00001 -> Do something
	sprintf(buffMsg, "%s ----> %s",currentDateTime().c_str(),strMsg);
	returnMsg = Trimwhitespace(buffMsg);
	return returnMsg;
}
string GenerateErrorLogMsg(exception& e)
{
	char buffMsg[1024];
	string returnMsg;
	// 2014/09/30 11:11:11 --> T00001 -> Do something
	sprintf(buffMsg, "%s ----> ServiceCameraTrigger::Error ---> %s",currentDateTime().c_str(),e.what());
	returnMsg = Trimwhitespace(buffMsg);
	return returnMsg;
}

TextOperation* TextOperation::UpdateCameraStatus(TextOperation *txtObj)
{
	GetRegValue(ENABLE_SEND_TO_HOST,"ENABLE_SEND_TO_HOST");
	GetRegValue(DEVCON_FLAG,"DEVCON_FLAG");
	GetRegValue(MAX_DEVCON,"MAX_DEVCON");
	
	GetCameraStatusRegValue(DEVCON_COUNT,"Devcon");
	GetCameraStatusRegValue(Camera1Status,"0001");
	GetCameraStatusRegValue(Camera2Status,"0002");
	if(strcmp(ENABLE_CAMERA_3,"TRUE"))
	{
		GetCameraStatusRegValue(Camera3Status,"0003");
	}
	if(strcmp(ENABLE_CAMERA_4,"TRUE"))
	{
		GetCameraStatusRegValue(Camera4Status,"0004");
	}
	return txtObj;
}
// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y/%m/%d %X", &tstruct);

    return buf;
}
const std::string currentDate() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y%m%d_", &tstruct);

    return buf;
}
std::string IntToString ( int number )
{
	std::string sReturn;
	char buffer [33];
	itoa (number,buffer,10);
	sReturn = buffer;
	return sReturn;
}
char *Trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace(*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace(*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}
string TextOperation::StringToUpper(string strToConvert)
{
    std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

    return strToConvert;
}