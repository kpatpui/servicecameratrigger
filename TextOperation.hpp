#pragma once
#include "windows.h"
#include <iostream>
#include <cstring>
#include <string>
#include <iomanip>
#include <fstream>
#include "tchar.h"
#include <vector>
#include "Filter.h"

#define SOFTWARE_CVService _T("SOFTWARE\\CVService")
#define SOFTWARE_CVService_Filter _T("SOFTWARE\\CVService\\Filter")
#define SOFTWARE_CVService_Status _T("SOFTWARE\\CVService\\Status")
#define SOFTWARE_Terminal_Addr _T("SOFTWARE\\Wincor Nixdorf\\ProAgent\\CurrentVersion\\SSTP")

class TextOperation {
  public:
	char LOG_FILE_PATH[1024];// default = "C:\\Program Files\\CVService\\";
	char LOG_FILE_NAME[1024];// default = "cameraTrigger.log";
	char XML_PATH[1024];// default = "D:\\LogArchive\\filter.xml";
	char SERVICE_NAME[1024];// default = "CVService";
	char TRANSACTION_FILTER[1024];//// default = "D:\\LogArchive\\filter.ini";
	char TerminalID[1024];
	char IDLE_TIME[128];
	char IDLE_DELAY_TIME[128];
	char PRESENTING_DELAY_TIME[128];
	char PRESENTING_FIRST_TIME_DELAY_TIME[128];
	
	char MAX_DEVCON[1024];
	char Camera1Status[1024];
	char Camera2Status[1024];
	char Camera3Status[1024];
	char Camera4Status[1024];
	
	char ENABLE_CAMERA_1[1024];
	char ENABLE_CAMERA_2[1024];
	char ENABLE_CAMERA_3[1024];
	char ENABLE_CAMERA_4[1024];
	
	char DEVCON_COUNT[1024];
	char ENABLE_SEND_TO_HOST[1024];
	char DEVCON_FLAG[1024];

    TextOperation()
	{
		GetRegValue(LOG_FILE_PATH,"LOG_FILE_PATH");
		GetRegValue(LOG_FILE_NAME,"LOG_FILE_NAME");
		GetRegValue(XML_PATH,"XML_PATH");
		GetRegValue(SERVICE_NAME,"CVServiceName");
		GetRegValue(TRANSACTION_FILTER,"TRANSACTION_FILTER");
		GetRegValue(IDLE_TIME,"IDLE_TIME");
		GetRegValue(IDLE_DELAY_TIME,"IDLE_DELAY_TIME");
		GetRegValue(PRESENTING_DELAY_TIME,"PRESENTING_DELAY_TIME");
		GetRegValue(PRESENTING_FIRST_TIME_DELAY_TIME,"PRESENTING_FIRST_TIME_DELAY_TIME");
		GetRegValue(ENABLE_CAMERA_1,"ENABLE_CAMERA_1");
		GetRegValue(ENABLE_CAMERA_2,"ENABLE_CAMERA_2");
		GetRegValue(ENABLE_CAMERA_3,"ENABLE_CAMERA_3");
		GetRegValue(ENABLE_CAMERA_4,"ENABLE_CAMERA_4");
		GetRegValue(ENABLE_SEND_TO_HOST,"ENABLE_SEND_TO_HOST");
		GetRegValue(DEVCON_FLAG,"DEVCON_FLAG");
		GetRegValue(MAX_DEVCON,"MAX_DEVCON");
		
		GetTerminalIdRegValue(TerminalID,"TerminalID");
		
		// status
		GetCameraStatusRegValue(DEVCON_COUNT,"Devcon");
		if(strcmp(ENABLE_CAMERA_1,"TRUE") == 0)
		{
			GetCameraStatusRegValue(Camera1Status,"0001");
		}
		if(strcmp(ENABLE_CAMERA_2,"TRUE") == 0)
		{
			GetCameraStatusRegValue(Camera2Status,"0002");
		}
		if(strcmp(ENABLE_CAMERA_3,"TRUE") == 0)
		{
			GetCameraStatusRegValue(Camera3Status,"0003");
		}
		if(strcmp(ENABLE_CAMERA_4,"TRUE") == 0)
		{
			GetCameraStatusRegValue(Camera4Status,"0004");
		}

	};
	~TextOperation()
	{
		// TODO: check to close paramemet before close obj.
	};
	
	bool WriteXml(Filter,std::string,std::string,std::string,std::string);
	long GetRegValue(char output[],char *key);
	long SetRegValue(char *pValue,char *keys);
	long GetTerminalIdRegValue(char output[],char *key);
	long GetCameraStatusRegValue(char output[],char *key);
	int WriteToLog(char msg[]);
	int WriteCameraStatusLog(char msg[]);
	char *Trimwhitespace(char *str);
	static std::string TextOperation::StringToUpper(std::string strToConvert);
	static long TextOperation::GetFilterRegValue(char *pValue,char *keys);
	TextOperation* TextOperation::UpdateCameraStatus(TextOperation *txtObj);
};

