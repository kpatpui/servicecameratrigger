#ifndef DCSTTFW_HPP
#define DCSTTFW_HPP
/*
    I N C L U D E S
*/
#include "ccFrmw.hpp"
#include "collect.hpp"
#include "ccIniPar.hpp"
#include "ccUti_e.h"
#include "cctimdat.hpp"
#include "dcjouent.hpp"
#include "time.h"

#include "cctrnact.xpp"
#include "ccVarFw.xpp"
#include "CCSyncFw.xpp"
#include "cccdmfw.xpp"
#include "ccApplic.xpp"
#include "ccDialog.xpp"
#include "ccJourna.xpp"
#include "ccDataFW.xpp"
#include "dcdatafw.xpp"
#include "dcDialog.xpp"
#include "dcStepFW.xpp"
#include "dcApplFW.xpp"
#include "winfp.xpp"

#include <string>
#include <vector>

using namespace std;

/*
    D E F I N E S
*/
#define TRC_MODID_STTTMPL       1000
#define TRC_FUNC                10
#define TRC_INFO                11
#define TRC_DETAIL              12

#define THIS_STEP_FW        ServiceCameraTrigger::Current()
#define VAR_FW              ServiceCameraTrigger::Current().varFwM
#define DIALOG_FW          	ServiceCameraTrigger::Current().dlgFwM
#define DATA_FW				ServiceCameraTrigger::Current().dataFwM
#define STEP_FW             ServiceCameraTrigger::Current().stepFwM
#define PCMOS_FW            ServiceCameraTrigger::Current().pcmosFwM
#define FP_FW               ServiceCameraTrigger::Current().FpFwM
#define JOURNAL_FW			ServiceCameraTrigger::Current().journalFwM


#ifdef CC_W32
#pragma pack(push, 4)
#else
#pragma pack(4)
#endif


class Step : public CCObject
{
    public:
        SHORT FrmSendEvent(SHORT, PVOID = 0, SHORT = 0);
        SHORT FrmSendEventSync(SHORT, PVOID = 0, SHORT = 0);
        SHORT FrmEnterCritSec();
        SHORT FrmExitCritSec();
        SHORT ProcessStep(PCHAR pchSectNextStep, PCHAR szStepFunc,
                                                        PCHAR szStepParams);
        SHORT ProcessBaseStep(PCHAR pchSectNextStep, PCHAR szStepFunc,
                                                        PCHAR szStepParams);
        SHORT ProcessStepAsync(CCFRMW_JOB_STATUS *pJobState,
                PCHAR pchSectNextStep, PCHAR szStepFunc, PCHAR szStepParams);
        BOOL FrmCheckAsyncCompletion(CCFRMW_JOB_STATUS *, ULONG ulTimeOut);
        /*
            Following methods have to be implemented
        */
        virtual SHORT Process(PCHAR pchSectNextStep, PCHAR szStepParams) = 0;
        virtual PCHAR FuncName() = 0;
};

class ServiceCameraTrigger : public CCFrameWork
{
    private:
        static ServiceCameraTrigger *pThisM;
        CCObArray coaStepsM;
        SHORT AddStep(Step *pStep);
        VOID InitStepList();

    public:
        ServiceCameraTrigger();
		~ServiceCameraTrigger();
        static ServiceCameraTrigger &Current();
        virtual SHORT OnFrmEvent(PCHAR, SHORT, VOID *, SHORT);
        virtual SHORT OnFrmRequest(SHORT, VOID *, SHORT,
                                        VOID *, SHORT, VOID *, SHORT, ULONG);
        SHORT ProcessStep(PCHAR, PCHAR, PCHAR);
		CCJournalFW Journal_FW;
		CCVarFW		Var_FW;
		dcDataFW	dataFwM;
		CCCmos		pcmosFwM;
		WinFpFW     FpFwM; 
        NdcDdcDialogFW			dlgFwM;
        NdcDdcStepFW			stepFwM;
		CCApplicationFW         applFwM;
		VOID ThreadFunc();
		
		//idle thread
		HCC_THREAD hThread;
		
		// presenting thread
		HCC_THREAD prensentingThread;
		
		SHORT usMsgNo;
};

class PrtParam
{
    private:
        CCObArray coaParamM;

    public:
        PrtParam(PCHAR);
        ~PrtParam();

        PCHAR Param(int) const;
		SHORT GetSize();
};

class CCStringObject : public CCObject , public CCString
{
    public:
        CCStringObject() : CCString() {};
        CCStringObject(PCHAR szTxt) : CCString(szTxt) {};
        CCStringObject(PCHAR szTxt, SHORT sLen) : CCString(szTxt, sLen) {};
};


#ifdef CC_W32
#pragma pack(pop)
#else
#pragma pack()
#endif

#endif
