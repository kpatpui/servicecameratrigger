#include "SERVICE_CAMERA_TRIGGER.hpp"
#include <stdio.h>
#include <stdlib.h>

#include <vector>

#include "SttTmpl.hpp"
#include "trcerr_e.h"
#include "filter.h"
#include "EjSplitData.hpp"
#include "CashPresenting.hpp"

// local prototype
string StringToUpper(string strToConvert);
Filter& FindFilter(vector<Filter> vect,string sKey);

SC_HANDLE schSCManager;
SC_HANDLE schService;
char buffMsg[1024];

SERVICE_CAMERA_TRIGGER::SERVICE_CAMERA_TRIGGER(void)
{
	TrcWritef(TRC_FUNC, "> SERVICE_CAMERA_TRIGGER::SERVICE_CAMERA_TRIGGER(void)");
}

SERVICE_CAMERA_TRIGGER::~SERVICE_CAMERA_TRIGGER(void)
{
	free(buffMsg);

	CloseServiceHandle(schService); 
    CloseServiceHandle(schSCManager);
	TrcWritef(TRC_FUNC, "< SERVICE_CAMERA_TRIGGER::SERVICE_CAMERA_TRIGGER(void)");
}

void SERVICE_CAMERA_TRIGGER::DoTriggerSvc(PCHAR data,vector<Filter> filter, TextOperation *txtObj)
{
	TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc");
	
	bool isSuccess = true;
	bool isFilterFound = false;
	int selectMode = 0;
	
	std::string sCode = "0";
	std::string sFilterDesc = "0";
	bool sCamera1 = false;
	bool sCamera2 = false;
	bool sCamera3 = false;
	bool sCamera4 = false;
	std::string sType = "T";
	string strMsg = "DoTriggerSvc";

	std::string sTerminalId = txtObj->TerminalID;
	std::string descString = StringToUpper(data);

	EjSplitData *ejData = new EjSplitData(data);
	Filter filterObj;

	filterObj = FindFilter(filter,data);
	
	if(filterObj.GetID() != 0)
	{
		char buffMsg[1024];
		sprintf(buffMsg, "<<-------------START------------->>");

		string strMsg = "DoTriggerSvc";
		txtObj->WriteToLog (buffMsg);

		sprintf(buffMsg, "%s %s data: %s", ejData->TerminalID.c_str(), strMsg.c_str(),descString.c_str());
		txtObj->WriteToLog (buffMsg);

		isFilterFound = true;

		sTerminalId = ejData->TerminalID;
		sCode = filterObj.GetID();
		sFilterDesc = filterObj.GetDesc();
		sCamera1 = filterObj.GetisCapDev1();
		sCamera2 = filterObj.GetisCapDev2();
		sCamera3 = filterObj.GetisCapDev3();
		sCamera4 = filterObj.GetisCapDev4();
		sType = filterObj.GetTranType();
		
		sprintf(buffMsg, "%s %s data: %s", sTerminalId.c_str(), strMsg.c_str(),sFilterDesc.c_str());
		txtObj->WriteToLog (buffMsg);
		/*sprintf(buffMsg, "%s Code: %d Desc: %s", sTerminalId.c_str(), filterObj.GetID(), sFilterDesc.c_str());
		txtObj->WriteToLog (buffMsg);*/
	}

	if(isFilterFound)
	{
		sprintf(buffMsg, "%s Code: %d Desc: %s", sTerminalId.c_str(), filterObj.GetID(), sFilterDesc.c_str());
		txtObj->WriteToLog (buffMsg);

		TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc->Filter found id: %d",filterObj.GetID());

		SERVICE_STATUS_PROCESS ssp;
		DWORD dwBytesNeeded;
		// Get a handle to the SCM database. 
		schSCManager = OpenSCManager( 
			NULL,                    // local computer
			NULL,                    // ServicesActive database 
			SC_MANAGER_ALL_ACCESS);  // full access rights 
	 
		if (NULL == schSCManager) 
		{
			sprintf(buffMsg, "%s OpenSCManager failed (%d)", sTerminalId.c_str(), GetLastError());
			txtObj->WriteToLog (buffMsg);

			TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> OpenSCManager failed (%d)",GetLastError());
			isSuccess = false;
		}
		else
		{
			sprintf(buffMsg, "%s OpenSCManager OK", sTerminalId.c_str());
			txtObj->WriteToLog (buffMsg);

			TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> OpenSCManager OK");
		}
		
		if(isSuccess)
		{
			// Get a handle to the service.
			schService = OpenService( 
				schSCManager,         // SCM database 
				txtObj->SERVICE_NAME,            // name of service 
				SERVICE_ALL_ACCESS );  
		 
			if (schService == NULL)
			{ 
				sprintf(buffMsg, "%s OpenService failed (%d)", sTerminalId.c_str(), GetLastError());
				txtObj->WriteToLog (buffMsg);
				TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> OpenService failed (%d)\n", GetLastError());
				isSuccess = false;
			}    
			else
			{
				sprintf(buffMsg, "%s Schedule Service OK", sTerminalId.c_str());
				txtObj->WriteToLog (buffMsg);
				TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Schedule Service OK");
			}
		}
		if(isSuccess)
		{
			// Make sure the service is not already stopped.
			if ( !QueryServiceStatusEx( 
					schService, 
					SC_STATUS_PROCESS_INFO,
					(LPBYTE)&ssp, 
					sizeof(SERVICE_STATUS_PROCESS),
					&dwBytesNeeded ) )
			{
				sprintf(buffMsg, "%s QueryServiceStatusEx failed (%d)", sTerminalId.c_str(), GetLastError());
				txtObj->WriteToLog (buffMsg);
				TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> QueryServiceStatusEx failed (%d)", GetLastError()); 
				isSuccess = false;
			}
			else
			{
				sprintf(buffMsg, "%s QueryServiceStatusEx OK", sTerminalId.c_str());
				txtObj->WriteToLog (buffMsg);
				TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> QueryServiceStatusEx OK");
			}
		}

		// check status service
		
		if (isSuccess)
		{
			switch(ssp.dwCurrentState)
			{
				case SERVICE_STOPPED:
					sprintf(buffMsg, "%s Service is SERVICE_STOPPED", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_STOPPED");
					isSuccess = false;
					break;
				case SERVICE_START_PENDING:
					sprintf(buffMsg, "%s Service is SERVICE_START_PENDING", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_START_PENDING");
					isSuccess = false;
					break;
				case SERVICE_STOP_PENDING:
					sprintf(buffMsg, "%s Service is SERVICE_STOP_PENDING", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_STOP_PENDING");
					isSuccess = false;
					break;
				case SERVICE_CONTINUE_PENDING:
					sprintf(buffMsg, "%s Service is SERVICE_CONTINUE_PENDING", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_CONTINUE_PENDING");
					isSuccess = false;
					break;
				case SERVICE_PAUSE_PENDING:
					sprintf(buffMsg, "%s Service is SERVICE_PAUSE_PENDING", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_PAUSE_PENDING");
					isSuccess = false;
					break;
				case SERVICE_PAUSED:
					sprintf(buffMsg, "%s Service is SERVICE_PAUSED", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_PAUSED");
					isSuccess = false;
					break;
				case SERVICE_RUNNING:
					sprintf(buffMsg, "%s Service is SERVICE_RUNNING", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is SERVICE_RUNNING");
					isSuccess = true;
					break;
				default:
					sprintf(buffMsg, "%s Service is default", sTerminalId.c_str());
					txtObj->WriteToLog (buffMsg);
					TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service is Something?.");
					isSuccess = true;
					break;
			}
		}

		if(isSuccess)
		{
			txtObj->WriteXml(filterObj,ejData->Date,ejData->Time,ejData->Description,ejData->TerminalID);

			// If a stop do nothing.
			if ( !ControlService( 
					schService, 
					SNAP_SHOT, 
					(LPSERVICE_STATUS) &ssp ) )
			{
				DWORD errorNumber = GetLastError();
				sprintf(buffMsg, "%s ControlService failed (%d)", sTerminalId.c_str(), errorNumber);
				txtObj->WriteToLog (buffMsg);
				TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> ControlService failed (%d)", errorNumber);
				// disable code do nothing.
				// manual maintenance
				//if(errorNumber == 1053)
				//{
				//	// do something with service
				//	txtObj->WriteToLog ("TODO: Do something with service.");
				//}
			}
			else
			{
				TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> (%d)", GetLastError());
			}
			TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service Trigger successfully");
		}
		else
		{
			TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::DoTriggerSvc -> Service Trigger failed.");
		}
		sprintf(buffMsg, "<<-------------END------------->>");
		txtObj->WriteToLog (buffMsg);
	}
	else{
		sprintf(buffMsg, "Filter not found: %s",data);
		txtObj->WriteToLog (buffMsg);
	}
	
	TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::DoTriggerSvc");
}

string StringToUpper(string strToConvert)
{
    std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

    return strToConvert;
}

Filter& FindFilter(vector<Filter> filter,string sKey) 
{
	  string filterDesc;
	  static Filter returnObj;
	  int msgid;
	  SHORT sRc;
      string isTran;
	  bool found = false;
	  int i =0;
	  for each(Filter filterObj in filter) {
			filterDesc = filterObj.GetDesc();
			//Convert to upper case Key
			transform(sKey.begin(), sKey.end(),sKey.begin(), ::toupper);		
			//Finding in Filter if found Capture Image
			char *str;
			str=&filterDesc[0];
			*str = toupper(*str);
			char *p;
			p = strtok (str,"*");
			returnObj = filterObj;
			while (p != NULL){	
				if(sKey.find(p) != -1){
					if(strcmp(p,"CARD") == 0)
					{
						// skip word card at first time.
					}
					else
					{
						found = true;
						returnObj = filterObj;
						TrcWritef(TRC_FUNC, ">>>>ServiceCameraTrigger::filterDesc %s %",str);
						return returnObj;
					}
				}
				else{
					found = false;
					returnObj.SetId(0);
					break;
				}	
				p = strtok (NULL, "*");
			}
	  }
		returnObj.SetId(0);
	return returnObj;
}