// Filter.h: interface for the Filter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILTER_H__6D908882_3D10_465C_922D_FB10A8A84466__INCLUDED_)
#define AFX_FILTER_H__6D908882_3D10_465C_922D_FB10A8A84466__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

using namespace std;
class Filter  
{
	private:
		int msgID;
		string msgDesc;
		bool isCapDev1;
		bool isCapDev2;
		bool isCapDev3;
		bool isCapDev4;
		string TranType;
	public:
		Filter();
		virtual ~Filter();
		void SetId(int );
		void SetDesc(string );
		void SetisCapDev1(bool );
		void SetisCapDev2(bool );
		void SetisCapDev3(bool );
		void SetisCapDev4(bool );
		void SetTranType(string );
		int GetID();
		string GetDesc();
		bool GetisCapDev1();
		bool GetisCapDev2();
		bool GetisCapDev3();
		bool GetisCapDev4();
		string GetTranType();
		static vector<Filter> ImplementFilter();
		static vector<Filter> QueryFilterKey(HKEY hKey);

};

#endif // !defined(AFX_FILTER_H__6D908882_3D10_465C_922D_FB10A8A84466__INCLUDED_)
