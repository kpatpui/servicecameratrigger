/*
    I N C L U D E S
*/
#include <stdio.h>
#include <stdlib.h>

#include "SttTmpl.hpp"
#include "IdleThread.hpp"
#include "SERVICE_CAMERA_TRIGGER.hpp"
#include "MessageData.hpp"

#include "trcerr_e.h"

NdcDdcApplicationFW		APPL_FW;

SERVICE_CAMERA_TRIGGER *mainAtIdle;
MessageData *msgObj;

const std::string currentEjDateTime();

IdleThread::IdleThread(void)
{
	TrcWritef(TRC_INFO, "> IdleThread::Constructor");
	mainAtIdle = new SERVICE_CAMERA_TRIGGER;
	msgObj = new MessageData;
}

IdleThread::~IdleThread(void)
{
	TrcWritef(TRC_INFO, "> IdleThread::Destructor");
	free(mainAtIdle);
	free(msgObj);
}

string IdleThread::DoSomeString(string inputTxt)
{
	this->testStr = this->testStr + "_" +inputTxt;
	return this->testStr;
}
char IdleThread::IdleWait()
{
	//InitAllocCheck();

	TrcWritef(TRC_INFO, "> IdleThread::IdleWait()");
	int	idle_time = atoi(txtObj->IDLE_TIME);
	int idle_delay_time = atoi(txtObj->IDLE_DELAY_TIME);
	int IDLE_MSG_NUMBER = 9000;
	int CAMERA_LOG_MSG_NUMBER = 9050;

	while(this->bRunning)
	{
		time_t nowTime;
		time(&nowTime);
		double diff = difftime(nowTime,this->startTime);

		if(diff >= idle_time)
		{

			string currentDateTime = currentEjDateTime();
			TrcWritef(TRC_INFO, " IdleThread::Diff: %.f, Now Trigger.",diff);
			char data[1024];
			sprintf(data, "%s %s IDLE SNAPSHOT",currentDateTime.c_str(), txtObj->TerminalID);
			int isFoundCameraError = CAMERA_STATUS_OK;
			BOOL isFoundCamera = FALSE;
			int cameraErrorStatus = 1;

			txtObj = txtObj->UpdateCameraStatus(txtObj);
			isFoundCamera = msgObj->CheckStatusChange(txtObj,Journal_FW,Var_FW);
			
			if(isFoundCamera)
			{
				msgObj->WriteJournal(IDLE_MSG_NUMBER,Journal_FW);
				BOOL bIsCameraError = msgObj->CheckErrorStatusChange(txtObj);
				if(bIsCameraError == TRUE)
				{
					if(msgObj->CheckConditionForSendMsgToHost(txtObj,Journal_FW))
					{
						msgObj->WriteJournal(CAMERA_LOG_MSG_NUMBER,Journal_FW);
						msgObj->SendMsgToHost("T2HUnsolicitedCamError");
					}
				}
			}

			TimeReset();
		}
		else
		{
			TrcWritef(TRC_INFO, " IdleThread::Diff: %.f, Not Now",diff);
			CCSleep(idle_delay_time);
		}
	}
	TrcWritef(TRC_INFO, "< IdleThread::IdleWait()");
	//DeInitAllocCheck();

	return 'E';
}

void IdleThread::TimeReset()
{
	 time(&this->startTime);
	this->endTime = startTime;
}

const std::string currentEjDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%d/%m/%Y %X", &tstruct);

    return buf;
}
