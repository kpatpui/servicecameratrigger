#include "windows.h"
#include <iostream>
#include <cstring>
#include <string>
#include <iomanip>
#include <fstream>
#include "tchar.h"
//#include <vector>

class EjSplitData
{
	public:
		EjSplitData(PCHAR inputData);
		~EjSplitData(void);

		std::string Date;
		std::string Time;
		std::string TerminalID;
		std::string Description;
};