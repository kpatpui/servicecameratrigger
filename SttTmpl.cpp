/*
    I N C L U D E S
*/
#include <stdio.h>
#include <stdlib.h>

#include "SttTmpl.hpp"
#include "SERVICE_CAMERA_TRIGGER.hpp"
#include "TextOperation.hpp"
#include "IdleThread.hpp"
#include "CashPresenting.hpp"
#include "Filter.h"

#include "MessageData.hpp"

#include "trcerr_e.h"

/*
    S T A T I C   G L O B A L   V A R I A B L E S
*/
static CHAR szModG[] = "$MOD$ 140929 1000 STTTMPL.DLL";
static CHAR szSegG[] = "$SEG$ 140929 1000 STTTMPL.CPP";

bool isTranStart=true;
IdleThread *idle;
CashPresenting *presenting;
GlobalObj *globalObj;
TextOperation *txtObj;

/*
    I M P L E M E N T A T I O N
    ===========================

    class ServiceCameraTrigger
*/
ServiceCameraTrigger *ServiceCameraTrigger::pThisM = 0;
VOID _Optlink CThreadFunc(PVOID pv);
void _Optlink PresentingThreadFunc(PVOID pv);

ServiceCameraTrigger::ServiceCameraTrigger()
{

    TrcErrInit(TRC_MODID_STTTMPL, szModG);
    TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::ServiceCameraTrigger");
	TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::IdleThread");
	txtObj = new TextOperation;
	// reset devcon flag
	txtObj->SetRegValue("TRUE","DEVCON_FLAG");

	globalObj = new GlobalObj;

	TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::IdleThread");
    FrmSetName(CCTAFW);
	FrmRegisterForEvents(CC_JRN_FW);
	FrmRegisterForEvents(CCCDMFW);
	FrmRegisterForEvents(CCAPPLFW);
    pThisM = this;

    TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::ServiceCameraTrigger");
}

ServiceCameraTrigger::~ServiceCameraTrigger()
{
	
    TrcErrInit(TRC_MODID_STTTMPL, szModG);
    TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::~ServiceCameraTrigger");
	TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::~IdleThread");
	if(idle != NULL)
	{
		delete(idle);
	}
	if(txtObj != NULL)
	{
		delete(txtObj);
	}
	if(globalObj != NULL)
	{
		delete(globalObj);
	}
	TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::~IdleThread");
    TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::~ServiceCameraTrigger");
}


ServiceCameraTrigger &ServiceCameraTrigger::Current()
{
    return (*pThisM);
}


SHORT ServiceCameraTrigger::AddStep(
        Step *pcoStep)
{
    SHORT sRc = CCFRMW_RC_OK;
    TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::AddStep(%s)", pcoStep->FuncName());
    coaStepsM.Add(pcoStep);
    TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::AddStep -> %d", sRc);
    return (sRc);
}


SHORT ServiceCameraTrigger::OnFrmEvent(
        PCHAR szSender,
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    SHORT sRc;

	char  chData[1024];
	CCString  strData;
    TrcWritef(TRC_INFO, "> ServiceCameraTrigger::OnFrmEvent %d from %s",
                                                        sEventId, szSender);
	
	if (strcmp(szSender,CC_JRN_FW)==0)
	{
		TrcWritef(TRC_INFO, "> ServiceCameraTrigger got the event sEventId = %d" ,sEventId); 
		if (sEventId == 1)
		{
			TrcWritef(TRC_INFO, "> ServiceCameraTrigger Journal Data = %s" ,(PCHAR)pData); 
			sprintf(chData,"%s",(CHAR*)pData);
			strData = chData;

			//check present thread 
			if((strData.includes("Cash presented")) ||
					(strData.includes("CASH PRESENTED")) ||
					(strData.includes("CDM ERROR"))	)
			{
				if(globalObj->bPresentingThread)
				{
					presenting->bRunning = false;
					globalObj->bPresentingThread = FALSE;
					TrcWritef(TRC_INFO, "CashPresenting::Thread Kill"); 
					CCThreadKill(THIS_STEP_FW.prensentingThread);
					delete(presenting);
				}
				
			}

			//Idle thread
			TrcWritef(TRC_INFO, "IdleThread::Thread Check"); 
			if((strData.includes("Transaction start")) ||
						(strData.includes("TRANSACTION START")))
			{
				if(globalObj->bIdleThread)
				{
					idle->bRunning = FALSE;
					idle->txtObj = txtObj;

					globalObj->bIdleThread = FALSE;

					TrcWritef (TRC_FUNC,"> IdleThread::Thread Kill");
					CCThreadKill(THIS_STEP_FW.hThread);
					delete(idle);
				}
			}
			else if((strData.includes("Transaction end")) ||
						(strData.includes("TRANSACTION END")))
			{
				// check for idle
				if(!globalObj->bIdleThread)
				{
					TrcWritef(TRC_INFO, "> IdleThread::TRANSACTION END");
					idle = new IdleThread;
					
					globalObj->bIdleThread = TRUE;
					isTranStart = false;
					THIS_STEP_FW.usMsgNo = 9000;
					TrcWritef(TRC_INFO, "> IdleThread::THIS_STEP_FW.usMsgNo = 9000"); 
					idle->filter = globalObj->vFilter;
					idle->txtObj = txtObj;
					
					idle->Journal_FW = &this->Journal_FW;
					idle->Var_FW = &this->Var_FW;

					idle->TimeReset();
					idle->bRunning = true;
					SHORT sRet = CCThreadBegin(&THIS_STEP_FW.hThread, CThreadFunc, 0, idle);
				}
				else
				{
					TrcWritef(TRC_INFO, "> IdleThread::Not NULL");
				}

				// check for presenting; in case cdm error.
				if(globalObj->bPresentingThread)
				{
					presenting->bRunning = false;
					globalObj->bPresentingThread = FALSE;
					TrcWritef(TRC_INFO, "CashPresenting::Thread Kill"); 
					CCThreadKill(THIS_STEP_FW.prensentingThread);
					delete(presenting);
				}
			
			}
			else if((strData.includes("Application started")) ||
							(strData.includes("APPLICATION STARTED")))
			{
				if(!globalObj->bIdleThread)
				{
					TrcWritef(TRC_INFO, "> IdleThread::APPLICATION STARTED");

					idle = new IdleThread;
					globalObj->bIdleThread = TRUE;

					THIS_STEP_FW.usMsgNo = 9000;
					TrcWritef(TRC_INFO, "> IdleThread::THIS_STEP_FW.usMsgNo = 9000"); 
					idle->filter = globalObj->vFilter;
					idle->txtObj = txtObj;

					idle->Journal_FW = &this->Journal_FW;
					idle->Var_FW = &this->Var_FW;
					
					idle->TimeReset();
					idle->bRunning = true;
					SHORT sRet = CCThreadBegin(&THIS_STEP_FW.hThread, CThreadFunc, 0, idle);
				}

				// send camera status
				int isFoundCameraError = 0;
				BOOL isFoundCamera = FALSE;
				MessageData *msgObj = new MessageData;
				txtObj = txtObj->UpdateCameraStatus(txtObj);
				isFoundCamera = msgObj->CheckStatusChange(txtObj,&this->Journal_FW,&this->Var_FW);
				
				if(isFoundCamera)
				{
					BOOL bIsCameraError = msgObj->CheckErrorStatusChange(txtObj);
					if(bIsCameraError == TRUE)
					{
						msgObj->WriteJournal(9050,&this->Journal_FW);
						msgObj->SendMsgToHost("T2HUnsolicitedCamError");
					}
					else
					{
						msgObj->WriteJournal(9051,&this->Journal_FW);
						// disable code: may enable when host ready for this msg
						//msgObj->SendMsgToHost("T2HUnsolicitedCamGood");
					}
				}

				free(msgObj);
			}
			else if((strData.includes("Application stoped")) ||
							(strData.includes("APPLICATION STOPED")))
			{
				if(globalObj->bPresentingThread)
				{
					delete(presenting);
				}
				if(globalObj->bIdleThread)
				{
					delete(idle);
				}
			}
			else if((strData.includes("Servicemode left")) ||
							(strData.includes("SERVICEMODE LEFT")))
			{
				int isFoundCameraError = 0;
				BOOL isFoundCamera = FALSE;
				MessageData *msgObj = new MessageData;
				txtObj = txtObj->UpdateCameraStatus(txtObj);
				isFoundCamera = msgObj->CheckStatusChange(txtObj,&this->Journal_FW,&this->Var_FW);
				
				if(isFoundCamera)
				{
					BOOL bIsCameraError = msgObj->CheckErrorStatusChange(txtObj);
					if(bIsCameraError == TRUE)
					{
						msgObj->WriteJournal(9050,&this->Journal_FW);
						msgObj->SendMsgToHost("T2HUnsolicitedCamError");
					}
					else
					{
						msgObj->WriteJournal(9051,&this->Journal_FW);
						// disable code: may enable when host ready for this msg
						//msgObj->SendMsgToHost("T2HUnsolicitedCamGood");
					}
				}

				free(msgObj);
			}

			SERVICE_CAMERA_TRIGGER::DoTriggerSvc((PCHAR)pData,globalObj->vFilter,txtObj);

		}
	}
	else if (stricmp(szSender, CCCDMFW)==0)
    {
		switch (sEventId)
        {
			case CCCDMFW_EVT_END_DISPENSE:

				if(!globalObj->bPresentingThread)
				{
					presenting = new CashPresenting;
				}
				globalObj->bPresentingThread = TRUE;
				presenting->txtObj = txtObj;
				presenting->filter = globalObj->vFilter;
				presenting->bRunning = true;
				SHORT sRet = CCThreadBegin(&THIS_STEP_FW.prensentingThread, PresentingThreadFunc, 0, presenting);
			break;
		}
	}

    sRc = OnFrmEventBaseClass(szSender, sEventId, pData, sDataLen);

    TrcWritef(TRC_INFO, "< ServiceCameraTrigger::OnFrmEvent");
    return(CCFRMW_RC_OK);  

}

//VOID _Optlink CheckAndSendHostMsg()
//{
//	// send camera status
//	int isFoundCameraError = 0;
//	BOOL isFoundCamera = FALSE;
//	MessageData *msgObj = new MessageData;
//	txtObj = txtObj->UpdateCameraStatus(txtObj);
//	isFoundCamera = msgObj->CheckStatusChange(txtObj,&this->Journal_FW,&this->Var_FW);
//	
//	if(isFoundCamera)
//	{
//		BOOL bIsCameraError = msgObj->CheckErrorStatusChange(txtObj);
//		if(bIsCameraError == TRUE)
//		{
//			msgObj->WriteJournal(9050,&this->Journal_FW);
//			msgObj->SendMsgToHost("T2HUnsolicitedCamError");
//		}
//		else
//		{
//			msgObj->WriteJournal(9051,&this->Journal_FW);
//			msgObj->SendMsgToHost("T2HUnsolicitedCamGood");
//		}
//	}
//
//	free(msgObj);
//}

VOID _Optlink CThreadFunc(PVOID pv)
{
    TrcWritef(TRC_INFO, "> IdleThread::Start IdleWait");
    IdleThread * pClass;
    pClass=(IdleThread *) pv;

	char testA = pClass->IdleWait();

    TrcWritef(TRC_INFO, "< IdleThread::End IdleWait");
}

VOID _Optlink PresentingThreadFunc(PVOID pv)
{
    TrcWritef(TRC_INFO, "> CashPresenting::Start Presenting");
    CashPresenting * pClass;
    pClass=(CashPresenting *) pv;

	pClass->TriggerCashPresenting();

    TrcWritef(TRC_INFO, "< CashPresenting::End Presenting");
}

SHORT ServiceCameraTrigger::OnFrmRequest(
        SHORT sMethodId,
        VOID *pData1,
        SHORT sDataLen1,
        VOID *pData2,
        SHORT sDataLen2,
        VOID *pData3,
        SHORT sDataLen3,
        ULONG ulTimeOut)
{
    SHORT sRc;

    TrcWritef(TRC_INFO, "> ServiceCameraTrigger::OnFrmRequest %d", sMethodId);

    switch(sMethodId)
    {
		case CCTAFW_FUNC_INIT_RESOURCES:
        
			sRc = OnFrmRequestBaseClass(sMethodId, pData1, sDataLen1,
                            pData2, sDataLen2, pData3, sDataLen3, ulTimeOut);
            if ((sRc == CCFRMW_RC_FUNCTION_NOT_SUPPORTED) ||
                (sRc == CCFRMW_RC_OK))
            {
                InitStepList();
            }
            break;

        case CCTAFW_FUNC_PROCESS_STEP:
            sRc = ProcessStep((PCHAR)pData1, (PCHAR)pData2, (PCHAR)pData3);
            break;

        default:
            sRc = OnFrmRequestBaseClass(sMethodId, pData1, sDataLen1,
                            pData2, sDataLen2, pData3, sDataLen3, ulTimeOut);
            break;

    }

    TrcWritef(TRC_INFO, "< ServiceCameraTrigger::OnFrmRequest -> %d", sRc);
    return(sRc);
}



SHORT ServiceCameraTrigger::ProcessStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    int iIdx = 0;
    int iNumSteps = coaStepsM.GetSize();
    BOOL fFound = FALSE;
    unsigned int uiStepLen = strlen(szStepFunc);
    Step *pcoStep;
    SHORT sRc;

    TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::ProcessStep(%s, %s, %s)",
                                pchSectNextStep, szStepFunc, szStepParams);

    while (!fFound && (iIdx < iNumSteps))
    {
        pcoStep = (Step *)coaStepsM.GetAt(iIdx);
        if ((uiStepLen == strlen(pcoStep->FuncName())) &&
            (strcmp(szStepFunc, pcoStep->FuncName()) == 0))
        {
            fFound = TRUE;

            TrcWritef(TRC_INFO, "Found index %d, calling Process", iIdx);

            sRc = pcoStep->Process(pchSectNextStep, szStepParams);
        }
        else
        {
            iIdx++;
        }
    }

    if (!fFound)
    {
        TrcWritef(TRC_INFO, "Not found, calling base class");

        sRc = OnFrmRequestBaseClass(CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1, 0);
    }
    if (sRc == CCTAFW_RC_JUMP_LABEL)
    {
        if (!*pchSectNextStep || !strcmp(pchSectNextStep, "000"))
        {
            strcpy(pchSectNextStep, "TRAN_END");    // For now !!!!!
        }
    }
    TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::ProcessStep -> %d, next %s",
                                                    sRc, pchSectNextStep);
    return(sRc);
}


/*
    I M P L E M E N T A T I O N
    ===========================

    class Step
*/
SHORT Step::FrmSendEvent(
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    return (ServiceCameraTrigger::Current().FrmSendEvent(sEventId,
                                                            pData, sDataLen));
}


SHORT Step::FrmSendEventSync(
        SHORT sEventId,
        VOID *pData,
        SHORT sDataLen)
{
    return (ServiceCameraTrigger::Current().FrmSendEventSync(sEventId,
                                                            pData, sDataLen));
}


SHORT Step::FrmEnterCritSec()
{
    return (ServiceCameraTrigger::Current().FrmEnterCritSec());
}


SHORT Step::FrmExitCritSec()
{
    return (ServiceCameraTrigger::Current().FrmExitCritSec());
}


SHORT Step::ProcessStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    return (ServiceCameraTrigger::Current().FrmResolve(CCTAFW,
                                CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1));
}


SHORT Step::ProcessBaseStep(
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    return (ServiceCameraTrigger::Current().OnFrmRequestBaseClass(
                                CCTAFW_FUNC_PROCESS_STEP,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1, 0));

}


SHORT Step::ProcessStepAsync(
        CCFRMW_JOB_STATUS *pJobState,
        PCHAR pchSectNextStep,
        PCHAR szStepFunc,
        PCHAR szStepParams)
{
    return (ServiceCameraTrigger::Current().FrmAsyncResolve(CCTAFW,
                                CCTAFW_FUNC_PROCESS_STEP, pJobState,
                                pchSectNextStep, CCTAFW_SECT_NEXT_MAX_LEN,
                                szStepFunc, strlen(szStepFunc) + 1,
                                szStepParams, strlen(szStepParams) + 1));
}




BOOL Step::FrmCheckAsyncCompletion(
        CCFRMW_JOB_STATUS *pJobState,
        ULONG ulTimeOut)
{
    return (ServiceCameraTrigger::Current().FrmCheckAsyncCompletion(
                                                    pJobState, ulTimeOut));
}

/*
    I M P L E M E N T A T I O N
    ===========================

    CreateFrameWorkInstance
*/
SHORT _Export CreateFrameWorkInstance(
        PCHAR szFrameWorkName,
        CCFrameWork **ppFW)
{
    SHORT sRc;

    if (stricmp(szFrameWorkName, CCTAFW) == 0)
    {   /* create an instance of ServiceCameraTrigger */
        *ppFW = (CCFrameWork *)new ServiceCameraTrigger;

        sRc = 0;
    }
    else
    {
        sRc = -1;
    }

    return(sRc);
}
