/*
    I N C L U D E S
*/
#include "SttStep.hpp"
#include "trcerr_e.h"

/*
    S T A T I C   G L O B A L   V A R I A B L E S
*/
static CHAR szSegG[] = "$SEG$ 140923 1000 STTSTEP.CPP";

/*
    I M P L E M E N T A T I O N
    ===========================

    Methode InitStepList from class ServiceCameraTrigger
*/
VOID ServiceCameraTrigger::InitStepList()
{
    static BOOL fStepsAdded = FALSE;
    Step *pStep;
    SHORT sRc;

    TrcWritef(TRC_FUNC, "> ServiceCameraTrigger::InitStepList");
    /*
        Now create one instance of every step class and add it to the list
    */
    if (!fStepsAdded)
    {
        fStepsAdded = TRUE;
    }

	TrcWritef(TRC_FUNC, "< ServiceCameraTrigger::InitStepList");

    return;
}


PrtParam::PrtParam(
        PCHAR szParam)
{
    CCStringObject *pcoString;
    SHORT sPos;

    /*
        Scan parameters
    */
    while (szParam[0])
    {
        sPos = 0;

        while (szParam[sPos] &&
            (szParam[sPos] != '~'))
        {
            sPos++;
        }
        /*
            Got a parameter, store it
        */
        pcoString = new CCStringObject(szParam, sPos);
        coaParamM.Add(pcoString);
        /*
            Skip parameter and seperator
        */
        szParam += sPos;
        if (szParam[0] == '~')
        {
            szParam++;
        }
    }
}


PrtParam::~PrtParam()
{
    CCStringObject *pcoString;

    while (coaParamM.GetSize())
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(0);

        delete pcoString;

        coaParamM.RemoveAt(0);
    }
}


PCHAR PrtParam::Param(
        int iIdx) const
{
    CCStringObject *pcoString;
    PCHAR pchRc = "";

    if ((iIdx >= 0) && (iIdx < coaParamM.GetSize()))
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(iIdx);
        pchRc = pcoString->buffer();
    }

    return (pchRc);
}


SHORT PrtParam::GetSize() 
{
	SHORT sSize = (SHORT) coaParamM.GetSize();
	
	return(sSize);
}

DCStepParam::DCStepParam(PCHAR szParam)
{
    CCStringObject *pcoString;
    SHORT sLen;
    
    /*
        Scan parameters
    */
    while (szParam[0])
    {
        sLen = 1;

        while ((sLen < 3) && szParam[sLen])
        {
            sLen++;
        }
        /*
            Got a parameter, store it
        */
        pcoString = new CCStringObject(szParam, sLen);
        coaParamM.Add(pcoString);
        /*
            Skip parameter and seperator
        */
        szParam += sLen;
    }
}


DCStepParam::~DCStepParam()
{
    CCStringObject *pcoString;

    while (coaParamM.GetSize())
    {
        pcoString = (CCStringObject *)coaParamM.GetAt(0);

        delete pcoString;

        coaParamM.RemoveAt(0);
    }
}


