#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <sstream>
#include <vector>

#include "GlobalObj.hpp"
#include "Filter.h"

GlobalObj::GlobalObj(void)
{
	this->bIdleThread = FALSE;
	this->bPresentingThread = FALSE;
	this->vFilter = Filter::ImplementFilter();
}

GlobalObj::~GlobalObj(void)
{
}

