#pragma once

#include "Filter.h"
#include "TextOperation.hpp"

class CashPresenting
{
	public:
	CashPresenting(void);
	~CashPresenting(void);

	void CashPresenting::TriggerCashPresenting(void);

	//public param
	bool bRunning;
	bool bFirstTime;
	SERVICE_CAMERA_TRIGGER *mainAtPresenting;
	TextOperation *txtObj;
	vector<Filter> filter;
};
