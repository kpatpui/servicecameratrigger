#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <sstream>
#include <vector>
#include "EjSplitData.hpp"
#include "TextOperation.hpp"

// local prototype
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

EjSplitData::EjSplitData(PCHAR inputData)
{
	std::vector<std::string> tmpString = split(inputData, ' ');
	int iVectorCount = 0;
	for each(std::string str in tmpString) {
		switch(iVectorCount)
		{
			case 0:
				Date = str;
				break;
			case 1:
				Time = str;
				break;
			case 2:
				TerminalID = str;
				break;
			default:
				Description += str+" ";
				break;
		}
		iVectorCount++;
	}
}


EjSplitData::~EjSplitData()
{

}
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}