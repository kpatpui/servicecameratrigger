/*
    I N C L U D E S
*/
#include <stdio.h>
#include <stdlib.h>

#include "SttTmpl.hpp"
#include "SERVICE_CAMERA_TRIGGER.hpp"
#include "CashPresenting.hpp"

#include "trcerr_e.h"

//SERVICE_CAMERA_TRIGGER *mainAtPresenting;

CashPresenting::CashPresenting(void)
{
	TrcWritef(TRC_INFO, "> CashPresenting::Constructor");
	mainAtPresenting = new SERVICE_CAMERA_TRIGGER;
}

CashPresenting::~CashPresenting(void)
{
	TrcWritef(TRC_INFO, "> CashPresenting::Destructor");
	free(mainAtPresenting);
}

void CashPresenting::TriggerCashPresenting(void)
{
	//InitAllocCheck();

	TrcWritef(TRC_INFO, "> CashPresenting::TriggerCashPresenting()");
	this->bFirstTime = true;
	char data[64];
	int	iPresentingDelayTime = atoi(txtObj->PRESENTING_DELAY_TIME);
	int	iPresentingFirstTimeDelayTime = atoi(txtObj->PRESENTING_FIRST_TIME_DELAY_TIME);
	
	while(this->bRunning)
	{
		CCSleep(iPresentingFirstTimeDelayTime);
		sprintf(data, "01/01/2014 00:00:00 %s CASH PRESENTING",txtObj->TerminalID);
		mainAtPresenting->DoTriggerSvc((PCHAR)data,this->filter, txtObj);

		CCSleep(iPresentingDelayTime);
	}

	TrcWritef(TRC_INFO, "< CashPresenting::TriggerCashPresenting()");
	//DeInitAllocCheck();
}
