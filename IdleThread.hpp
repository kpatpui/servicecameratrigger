/*
    I N C L U D E S
*/
#include "ccIniPar.hpp"
#include "time.h"

#include "ccUti_e.h"
#include "cctimdat.hpp"
#include "dcjouent.hpp"

#include "dcdatafw.xpp"
#include "dcDialog.xpp"
#include "dcStepFW.xpp"
#include "dcApplFW.xpp"

#include "Filter.h"
#include "TextOperation.hpp"

#include <string>
#include <vector>

using namespace std;

class IdleThread
{
	public:
		// con and decon
		IdleThread(void);
		~IdleThread(void);

		// public method
		VOID IdleThread::ThreadFunc();
		char IdleThread::IdleWait();
		void IdleThread::TimeReset();
		string IdleThread::DoSomeString(string inputTxt);

		//public param
		bool bRunning;
		time_t startTime;
		time_t endTime;
		string testStr;
		int testInt;
		
		TextOperation *txtObj;
		vector<Filter> filter;
		CCJournalFW	*Journal_FW;
		CCVarFW	*Var_FW;
};
