#pragma once
#include "windows.h"
#include "winsvc.h"
#include "tchar.h"
#include "aclapi.h"
#include "string.h"
#import <msxml6.dll> rename_namespace(_T("MSXML"))
#include <conio.h>

#include <iostream>
#include <cstring>
#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include "Filter.h"
#include "TextOperation.hpp"
#include "GlobalObj.hpp"

#pragma comment(lib, "advapi32.lib")

#define SNAP_SHOT 200

class SERVICE_CAMERA_TRIGGER
{
	public:
		SERVICE_CAMERA_TRIGGER(void);
		~SERVICE_CAMERA_TRIGGER(void);
		//static void DoTriggerSvc(PCHAR);
		//static void DoTriggerSvc(PCHAR data,GlobalObj *globalObj);
		static void DoTriggerSvc(PCHAR data,vector<Filter> filter, TextOperation *txtObj);
		int getServiceStatus(void);
};
