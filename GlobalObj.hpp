
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <string>
#include <vector>
#include <algorithm>

#include "Filter.h"

class GlobalObj
{
	public:
		GlobalObj(void);
		~GlobalObj(void);
		
		void InitParam();

		std::vector<Filter> vFilter;
		BOOL bIdleThread;
		BOOL bPresentingThread;

};
